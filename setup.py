#!/usr/bin/env python
"""
[WHEN TO USE THIS FILE]
[INSTRUCTIONS FOR USING THIS FILE]

Project name: [MISSING]
Author: Micah Parks

This lives on the web at: [MISSING URL]
Target environment: python 3.7
"""

# Start standard library imports.
from setuptools import setup
# End standard library imports.

# Start third party imports.
# End third party imports.

# Start project imports.
# End project imports.


if __name__ == '__main__':
    setup(name='suv', packages=['suv'], version='developer')
