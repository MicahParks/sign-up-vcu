#!/usr/bin/env python
"""
[WHEN TO USE THIS FILE]
[INSTRUCTIONS FOR USING THIS FILE]

Project name: [MISSING]
Author: Micah Parks

This lives on the web at: [MISSING URL]
Target environment: python 3.7
"""

# Start standard library imports.
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from json import load
from os.path import basename
from smtplib import SMTP
from time import sleep, time
from typing import List
# End standard library imports.

# Start third party imports.
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.ui import Select
from pyvirtualdisplay import Display
# End third party imports.


JSON_FILE_PATH_STR = '/volume/signupvcu.json'
MAX_WAIT_SEC_INT = 5
MY_VCU_ADDRESS_STR = 'https://my.vcu.edu/myvcu-portlets/ssologin.do?applicationId=BANNER_SELF_SERVICE'
WAIT_UNTIL_NEXT_TRY_SEC_INT = 10
WEB_DRIVER_WAIT_OBJ = None


def email_screenshot(fromEmailStr: str, fromEmailPasswordStr: str, screenshotFilePathStr: str, toEmailStr: str,
                     emailHostAddress: str = 'smtp.gmail.com', portInt: int = 587) -> None:
    """
    """
    mimeMultipartObj = MIMEMultipart()
    mimeMultipartObj['From'] = fromEmailStr
    mimeMultipartObj['To'] = toEmailStr
    mimeMultipartObj['Date'] = formatdate(localtime=True)
    mimeMultipartObj['Subject'] = 'Classes sign up: screenshot confirmation'
    mimeMultipartObj.attach(MIMEText('Attached you will find a screenshot of what the web browser looked like after you'
                                     ' were automatically signed up for classes. Please validate it.'))
    with open(screenshotFilePathStr, 'rb') as inFile:
        screenshotMimeApplicationObj = MIMEApplication(inFile.read(), Name=basename(screenshotFilePathStr))
    screenshotMimeApplicationObj['Content-Disposition'] = 'attachment; filename="{}"'.format(
        basename(screenshotFilePathStr))
    mimeMultipartObj.attach(screenshotMimeApplicationObj)
    try:
        smtpSslObj = SMTP(host=emailHostAddress, port=portInt)
        smtpSslObj.ehlo()
        smtpSslObj.starttls()
        smtpSslObj.ehlo()
    except Exception as exceptionStr:
        print('Failure to connect to "{}:{}".\nException: "{}".'.format(emailHostAddress, portInt, exceptionStr))
        return
    try:
        smtpSslObj.login(user=fromEmailStr, password=fromEmailPasswordStr)
    except Exception as exceptionStr:
        print('Failure to log into "{}" at "{}:{}"\nException: "{}".'.format(fromEmailStr, emailHostAddress, portInt,
                                                                             exceptionStr))
    smtpSslObj.sendmail(fromEmailStr, toEmailStr, mimeMultipartObj.as_string())
    smtpSslObj.close()


def get_web_element(webDriverObj: WebDriver, xpathStr: str) -> WebElement:
    """
    """
    web_driver_wait(webDriverObj, xpathStr)
    elementObj = webDriverObj.find_element_by_xpath(xpathStr)
    webDriverObj.execute_script("arguments[0].scrollIntoView();", elementObj)
    return elementObj


def get_eid_password_str() -> str:
    """
    """
    with open('eidpassword.txt') as inFile:
        return inFile.read().strip()


def login_to_my_vcu(firefoxWebdriver: WebDriver, passwordStr: str, usernameStr: str):
    """
    """
    firefoxWebdriver.get(MY_VCU_ADDRESS_STR)
    eidInputXpathStr = '//*[@id="username"]'
    get_web_element(firefoxWebdriver, eidInputXpathStr).send_keys(usernameStr)
    passwordInputXpathStr = '//*[@id="password"]'
    get_web_element(firefoxWebdriver, passwordInputXpathStr).send_keys(passwordStr + '\ue007')
    return firefoxWebdriver


def logout_vcu_central_authentication(firefoxWebdriver: WebDriver) -> None:
    """
    """
    firefoxWebdriver.get('https://login.vcu.edu/cas/logout')


def main() -> None:
    """
    The logic of the file.
    """
    displayObj = Display(visible=0, size=(800, 600))
    displayObj.start()
    options = Options()
    options.headless = True
    firefoxWebdriver = webdriver.Firefox(options=options)
    with open(JSON_FILE_PATH_STR) as inFile:
        signUpVcuDict = load(inFile)
    peopleDictList = signUpVcuDict['peopleDictList']
    fromEmailStr = signUpVcuDict['fromEmailStr']
    fromEmailPasswordStr = signUpVcuDict['fromEmailPasswordStr']
    for nowIndexInt, nowPersonDict in enumerate(peopleDictList):
        crnStrList = nowPersonDict['crnStrList']
        passwordStr = nowPersonDict['passwordStr']
        usernameStr = nowPersonDict['usernameStr']
        print('Starting process for "{}".'.format(usernameStr))
        loginTimeFloat = time()
        login_to_my_vcu(firefoxWebdriver=firefoxWebdriver, passwordStr=passwordStr, usernameStr=usernameStr)
        while vcu_sign_up(crnStrList=crnStrList, firefoxWebdriver=firefoxWebdriver,
                          screenshotFilePathStr='{}.png'.format(usernameStr)) is False:
            if time() - loginTimeFloat > 1500:  # 25 minutes since login.
                print('Logging out an back in again...')
                logout_vcu_central_authentication(firefoxWebdriver=firefoxWebdriver)
                login_to_my_vcu(firefoxWebdriver=firefoxWebdriver, passwordStr=passwordStr, usernameStr=usernameStr)
            else:
                print('Sleeping for a bit...')
                sleep(WAIT_UNTIL_NEXT_TRY_SEC_INT)
            print('Trying again...')
            firefoxWebdriver.get('https://ssb.vcu.edu/proddad/twbkwbis.P_GenMenu?name=bmenu.P_MainMnu')
        firefoxWebdriver.get('https://login.vcu.edu/cas/logout')
        print('Signed up "{}".'.format(usernameStr))
    print('All done with sign ups. Time to send the emails.')
    for nowIndexInt, nowPersonDict in enumerate(peopleDictList):
        toEmailStr = nowPersonDict['toEmailStr']
        usernameStr = nowPersonDict['usernameStr']
        if toEmailStr is not False:
            emailBool = True
        else:
            emailBool = False
        if emailBool is True:
            email_screenshot(fromEmailStr=fromEmailStr, fromEmailPasswordStr=fromEmailPasswordStr,
                             screenshotFilePathStr='{}.png'.format(usernameStr), toEmailStr=toEmailStr)
            print('Email sent to "{}".'.format(toEmailStr))
    firefoxWebdriver.close()
    displayObj.stop()


def vcu_sign_up(crnStrList: List[str], firefoxWebdriver: WebDriver, screenshotFilePathStr: str) -> bool:
    """
    """
    print('Attempting to sign up...')
    studentTabXpathStr = '/html/body/div[2]/div[2]/table/tbody/tr[1]/td/table/tbody/tr/td[3]/a'
    get_web_element(firefoxWebdriver, studentTabXpathStr).click()
    firefoxWebdriver.get('https://ssb.vcu.edu/proddad/bwskfreg.P_AltPin')
    try:
        dropDownMenuXpathStr = '//*[@id="term_id"]'
        dropDownMenu = get_web_element(firefoxWebdriver, dropDownMenuXpathStr)
        dropDownMenuSelected = Select(dropDownMenu)
        try:
            dropDownMenuSelected.select_by_visible_text('Fall 2019')
            # dropDownMenuSelected.select_by_visible_text('Spring 2019')
        except NoSuchElementException:
            print("Couldn't find the right term from the drop down menu.")
            return False
    except TimeoutException as exceptionStr:
        # Automatically redirected to selected term.
        print('Sign ups are open, but you may not register yet.')
        return False
    submitButtonXpathStr = '/html/body/div[4]/form/input'
    get_web_element(firefoxWebdriver, submitButtonXpathStr).click()
    for nowIndexInt, nowCrnStr in enumerate(crnStrList):
        crnBoxXpathStr = '//*[@id="crn_id{}"]'.format(nowIndexInt + 1)
        try:
            get_web_element(firefoxWebdriver, crnBoxXpathStr).send_keys(nowCrnStr)
        except TimeoutException as exceptionStr:
            print('Sign ups are open, but you may not register yet.')
            return False
    submitButtonXpathStr = '/html/body/div[4]/form/input[19]'
    get_web_element(firefoxWebdriver, submitButtonXpathStr).click()
    sleep(.1)  # Wait for next page. Is there a better way to do this?
    currentScheduleXpathStr = '/html/body/div[4]/form/h3[1]'
    get_web_element(firefoxWebdriver, currentScheduleXpathStr)
    firefoxWebdriver.get_screenshot_as_file(screenshotFilePathStr)
    return True


def web_driver_wait(firefoxWebdriver: WebDriver, xpathStr: str) -> None:
    """
    Using "expected_conditions" you could wait for stuff to be clickable/usable by keyboard?
    """
    global WEB_DRIVER_WAIT_OBJ
    if WEB_DRIVER_WAIT_OBJ is None:
        WEB_DRIVER_WAIT_OBJ = WebDriverWait(firefoxWebdriver, MAX_WAIT_SEC_INT)
    WEB_DRIVER_WAIT_OBJ.until(expected_conditions.presence_of_element_located((By.XPATH, xpathStr)))


if __name__ == '__main__':
    main()
